package com.atlassian.jira.plugins.dvcs.analytics.smartcommits.event;

import com.atlassian.analytics.api.annotations.EventName;
import com.google.common.base.Preconditions;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.annotation.Nonnull;

public class SmartCommitEnabledByDefaultConfigEvent
{
    private final int organizationID;
    private final boolean smartCommitEnabledByDefault;
    private final String BASE_EVENT_NAME = "jira.dvcsconnector.smartcommit.config.default.%s";

    public SmartCommitEnabledByDefaultConfigEvent(@Nonnull final int organizationID, @Nonnull final boolean smartCommitEnabledByDefault)
    {
        Preconditions.checkNotNull(organizationID);
        Preconditions.checkNotNull(smartCommitEnabledByDefault);
        this.organizationID = organizationID;
        this.smartCommitEnabledByDefault = smartCommitEnabledByDefault;
    }

    @EventName
    public String determineEventName()
    {
        return String.format(BASE_EVENT_NAME, smartCommitEnabledByDefault ? "enabled" : "disabled");
    }

    public int getOrganizationID()
    {
        return this.organizationID;
    }

    @Override
    public boolean equals(Object o)
    {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode()
    {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this);
    }


}
